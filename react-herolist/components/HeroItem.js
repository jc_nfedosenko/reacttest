/**
 * React component for item in hero list
 */
var React = require('react');
var moment = require('moment');

var HeroItem = React.createClass({

  handleClick() {
      this.props.onRemove(this.props.name);
    },
    render() {

      var cn = "list-group-item";
      var geniusId = "geniusCheck" + this.props.index,
        powerId = "powerCheck" + this.props.index,
        visId = "visCheck" + this.props.index;

      return (
        <a className={cn}>
        {this.props.name}
        <span className="createdAt">{ moment(this.props.timestamp).fromNow() }</span>
        <span>
          <input type="checkbox" id={geniusId} checked={this.props.checks.genius} onChange={this.props.onChange}/><label htmlFor={geniusId} className="checkText">Genius</label>
          <input type="checkbox" id={powerId} checked={this.props.checks.power} onChange={this.props.onChange}/><label htmlFor={powerId} className="checkText">Superpower</label>
          <input type="checkbox" id={visId} checked={this.props.checks.invisible} onChange={this.props.onChange}/><label htmlFor={visId} className="checkText">Invisible</label>
        </span>
        <span className="glyphicon glyphicon-remove pull-right" onClick={this.handleClick}></span>
      </a>
      )

    }

});

module.exports = HeroItem;
