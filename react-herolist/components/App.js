/**
 * Main file of the app, which put things together, contains the major functionality
 */
var React = require('react');

var HeroList = require('./HeroList');
var HeroInput = require('./HeroInput');
var SideBar = require('./SideBar');
var App = React.createClass({
  /**
   * Setting default values to object parameters on first render
   * @return {Object} returns the initial values
   */
  getInitialState() {
      var heros = [],
        statistics = {
          genius: 0,
          power: 0,
          invisible: 0
        };
      if (localStorage.heros) {
        heros = JSON.parse(localStorage.heros);
      }
      heros.forEach(function (hero, index) {
        for (var skill in hero.skills) {
          if (hero.skills[skill] === true) {
            statistics[skill] += 1;
          }
        }
      });
      return {
        heros: heros,
        statistics: statistics
      };
    },
    /**
     * method for adding the hero to the array of heros and to local storage
     * @param {String} name the hero name
     * @param {Object} skills  contains the skills of a hero
     */
    addToFavorites(name, skills) {

      var heros = this.state.heros;
      if (!this.isHeroNameInFavorites(name)) {
        heros.push({
          name: name,
          timestamp: Date.now(),
          skills: skills
        });
        this.setState({
          heros: heros
        });
        localStorage.heros = JSON.stringify(heros);
        this.getSkillStatistics();
      }
    },
    /**
     * callback for checkbox on change
     * @param  {Event} event 
     */
    handleCheckBoxChange(event) {
      var heroIndex = event.target.id.replace(/^\D+/g, ''),
        heros = this.state.heros;
      if (event.target.id.indexOf("genius") > -1) {
        heros[heroIndex].skills.genius = !heros[heroIndex].skills.genius;
      }
      if (event.target.id.indexOf("power") > -1) {
        heros[heroIndex].skills.power = !heros[heroIndex].skills.power;
      }
      if (event.target.id.indexOf("vis") > -1) {
        heros[heroIndex].skills.invisible = !heros[heroIndex].skills.invisible;
      }
      console.log(heros[heroIndex].skills);
      localStorage.heros = JSON.stringify(heros);
      this.getSkillStatistics();
    },
    /**
     * method for removing hero from hero list
     * @param  {String} name hero name
     */
    removeFromFavorites(name) {

      var heros = this.state.heros;

      var index = -1;

      for (var i = 0; i < heros.length; i++) {

        if (heros[i].name == name) {
          index = i;
          break;
        }

      }

      // If it was found, remove it from the heros array

      if (index !== -1) {

        heros.splice(index, 1);
        console.log(heros);
        this.setState({
          heros: heros
        });
        localStorage.heros = JSON.stringify(heros);
        this.getSkillStatistics();
      }
    },
    /**
     * checks if the hero is already in out list
     * @param  {String}  name hero name
     * @return {Boolean} existance of hero in the list
     */
    isHeroNameInFavorites(name) {

      var heros = this.state.heros;

      for (var i = 0; i < heros.length; i++) {

        if (heros[i].name == name) {
          return true;
        }

      }
      return false;
    },
    /**
     * method for setting current statistics of hero's skills
     */
    getSkillStatistics() {
      var statistics = {
          genius: 0,
          power: 0,
          invisible: 0
        },
        heros = this.state.heros;
      heros.forEach(function (hero, index) {
        for (var skill in hero.skills) {
          if (hero.skills[skill] === true) {
            statistics[skill] += 1;
          }
        }
      });
      this.setState({
        statistics: statistics
      });
    },
    /**
     * rendering the component
     * @return {Object} the component react markup
     */
    render() {
      return (

        <div>
        <h1>Your Hero List</h1>

        <SideBar statistics={this.state.statistics}/>

        <HeroInput onSearch={this.addToFavorites}/>

        <HeroList locations={this.state.heros} onRemove={this.removeFromFavorites} onCheckBoxChange={this.handleCheckBoxChange}/>

      </div>

      );
    }

});

module.exports = App;
