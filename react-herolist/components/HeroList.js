/**
 * React component for list of heros
 */
var React = require('react');
var HeroItem = require('./HeroItem');

var HeroList = React.createClass({

  render() {

    var self = this;

    var locations = this.props.locations.map(function (l, index) {
      // Notice that we are passing the onClick callback of this
      // LocationList to each LocationItem.
      return <HeroItem name={l.name} timestamp={l.timestamp} 
       onRemove={self.props.onRemove} index={index} checks={l.skills} onChange={self.props.onCheckBoxChange}/>
    });

    if (!locations.length) {
      return null;
    }

    return (
      <div className="list-group col-xs-12 col-md-10 col-md-offset-1">
        <span className="list-group-item active">Saved Heros</span>
        {locations}
      </div>
    )

  }

});

module.exports = HeroList;
