# README #

This repository contains two realisation of simple hero list.

### Native React ###

On the branch react  you can find the version, made up using only react for all of the manipulations. 

### How do I get set up? ###

* Install modules
```
#!javascript

npm install

```

* To build project
```
#!javascript

npm run build

```
* Watch file changes
```
#!javascript

npm run watch

```

### React with Flux ###

on the branch flux you can find the same app, but with flux and react

### How do I get set up? ###

* Install modules
```
#!javascript

npm install

```

* To build project
```
#!javascript

npm run start

```