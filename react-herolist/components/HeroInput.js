/**
 * component for input field
 * @type {React Component}
 */
var React = require('react');

var HeroInput = React.createClass({
  getInitialState() {
      return {
        value: '',
        skills: {
          genius: false,
          power: false,
          invisible: false
        }
      };
    },
    /**
     * callback for submitting the input
     * @param  {Event} event triggered event
     */
    handleSubmit(event) {
      console.log(this.state.skills);
      event.preventDefault();
      // When the form is submitted, call the onSearch callback that is passed to the component

      this.props.onSearch(this.state.value, this.state.skills);

      // Unfocus the text input field
      this.getDOMNode().querySelector('input').blur();
      this.state.value = '';
      this.setState({
        skills: {
          genius: false,
          power: false,
          invisible: false
        }
      });

    },
    /**
     * callback for changing the input value and refreshing it
     * @param  {Event} event 
     */
    handleChange(event) {
      this.setState({
        value: event.target.value
      });
    },
    /**
     * callback on checkbox value changing
     * @param  {Event} event 
     */
    handleCheckBoxChange(event) {
      switch (event.target.id) {
      case "geniusCheck":
        this.state.skills.genius = !this.state.skills.genius;
        break;
      case "powerCheck":
        this.state.skills.power = !this.state.skills.power;
        break;
      case "visibleCheck":
        this.state.skills.invisible = !this.state.skills.invisible;
        break;
      }
      this.forceUpdate();
    },
    render: function () {
      return (
        <form id="adding_form" className="form-horizontal" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <div className="col-xs-12 col-md-6 col-md-offset-3">
            <div className="input-group">
              <input type="text" className="form-control" id="heroName" placeholder="Enter the hero name" 
              value={this.state.value} onChange={this.handleChange}/>
              <span>
                <input type="checkbox" id="geniusCheck" checked={this.state.skills.genius} onChange={this.handleCheckBoxChange}/><label htmlFor="geniusCheck" className="checkText">Genius</label>
                <input type="checkbox" id="powerCheck" checked={this.state.skills.power} onChange={this.handleCheckBoxChange}/><label htmlFor="powerCheck" className="checkText">Superpower</label>
                <input type="checkbox" id="visibleCheck" checked={this.state.skills.invisible} onChange={this.handleCheckBoxChange}/><label htmlFor="visibleCheck" className="checkText">Invisible</label>
              </span>

            </div>
          </div>
        </div>
      </form>
      );
    }
});

module.exports = HeroInput;
