/**
 * React component for bar with Statistics      
 */
var React = require('react');

var SideBar = React.createClass({
  render: function () {
    return (
      <div className="list-group col-xs-12 col-md-10 col-md-offset-1">
        <span className="list-group-item active">Statistics</span>
        <span className="col-xs-4">Genius: {this.props.statistics.genius}</span>
        <span className="col-xs-4">Superpower: {this.props.statistics.power}</span>
        <span className="col-xs-4">Invisible: {this.props.statistics.invisible}</span>
      </div>
    );
  }
});

module.exports = SideBar;
